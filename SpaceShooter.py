from tkinter import *
from math import sqrt
from random import shuffle
from time import *
from random import randint
import os

Health = 100
register = ''
Logo = ''
Logo1 = ''
begin = ''
buttonStart = ''
leader = ''
buttonLeader = ''
Control_Menu = ''
buttonControl = ''
a = 'Down'
b = 'Up'
c = 'Left'
d = 'Right'
keys = ''
keys2 = ''
wasd = ''
updown = ''
begin1 = ''
buttonStart1 = ''
firstname_entry = ''
firstname_entry1 = ''
register1 = ''
firstname_text = ''
firstname_text1 = ''
Score = 0
speed = 10
xas = 1
maxi = 70
Save = ''
Save1 = ''
Load = ''
Load1 = ''
bonusChance = 150
window = Tk()


def main():

    def Main(event):
        f = open("save.txt", "w")
        f.write("blank")
        f.close()
        os.remove("save.txt")
        canvas.delete(ALL)

        global Save, Score
        global Save1
        Save = PhotoImage(file="save.png")
        Save1 = Button(window, image=Save)
        Save1.place(x=900, y=0)
        Save1.bind('<ButtonPress-1>', Save2)
        global keys, keys2, wasd, updown, begin1, buttonStart1

        global buttonControl, firstname_entry, register, firstname_text
        global firstname_text1, register1, firstname_entry1, buttonStart1
        if firstname_entry != '':
            firstname_entry.destroy()
        if firstname_entry1 != '':
            firstname_entry1.destroy()
        if register != '':
            register.destroy()
        if register1 != '':
            register1.destroy()
        if firstname_text != '':
            firstname_text.destroy()
        if firstname_text1 != '':
            firstname_text1.destroy()
        if buttonStart1 != '':
            buttonStart1.destroy()
        if updown != '':
            updown.destroy()
        if wasd != '':
            wasd.destroy()
        global Logo1
        global buttonStart
        global buttonLeader
        global buttonControl
        Logo1.destroy()
        buttonStart.destroy()
        buttonLeader.destroy()
        buttonControl.destroy()
        width_c = window.winfo_screenwidth()
        height_c = window.winfo_screenheight()

        SpaceCraft = canvas.create_polygon(5, 5, 5, 25, 30, 15, fill="white")
        Armour = canvas.create_oval(0, 0, 30, 30, outline="blue")
        centre_x = width_c / 3
        centre_y = height_c / 3
        canvas.move(SpaceCraft, centre_x, centre_y)
# so space craft is in middle of screen
        canvas.move(Armour, centre_x, centre_y)
        global speed

        def spacecraft_move(event):
            global a, b, c, d
            pos = canvas.coords(SpaceCraft)
            width1 = window.winfo_screenwidth()
            height1 = window.winfo_screenheight()
            if event.keysym == a:
                if pos[1] < 940:
                    canvas.move(SpaceCraft, 0, speed)
                    canvas.move(Armour, 0, speed)
            elif event.keysym == b:
                if pos[1] > 10:
                    canvas.move(SpaceCraft, 0, -speed)
                    canvas.move(Armour, 0, -speed)
            elif event.keysym == c:
                if pos[0] > 5:
                    canvas.move(SpaceCraft, -speed, 0)
                    canvas.move(Armour, -speed, 0)
            elif event.keysym == d:
                if pos[0] < width1-42:
                    canvas.move(SpaceCraft, speed, 0)
                    canvas.move(Armour, speed, 0)

            elif event.keysym == "p":
                os.system("PAUSE")
                wait = canvas.create_text(325, 70, fill="white",
                                          font="Helvetica 20",
                                          text="Game will resume in 3 seconds")
                canvas.update()
                canvas.delete(wait)
                sleep(3)
            elif event.keysym == "space":
                os.system("PAUSE")
                wait = canvas.create_text(325, 70, fill="white",
                                          font="Helvetica 20",
                                          text="Game will resume in 3 seconds")
                canvas.update()
                canvas.delete(wait)
                sleep(3)
            elif event.keysym == "10":
                pos = canvas.coords(SpaceCraft)
                print(pos)
                canvas.move(Bullet, pos[0]+22, pos[1]+6)
                offset = Bullet, pos[0] + 22, pos[1] + 6
                window.after(100, shooting_after)
            elif event.keysym == "1":
                if event.keysym == "2":
                    print("hello")
        canvas.bind_all('<Key>', spacecraft_move)
        Speed_Enemy = 10
        MinSize_Bonus = 5
        MaxSize_Bonus = 15
        Min_Space = 5
        Max_Space = 7
        colours = ["grey", "white", "darkgrey", "pink", "lime"]
        Bonus_List = []
        BonusSize_List = []
        EnemySize_List = []
        Bonus_Speed = []
        Enemy_List = []
        Enemy_Speed = []
        Distance = []
        global xas, maxi
        xas = 1
        maxi = 70

        def Bonus():
            for i in range(xas):
                y = 0
                x = randint(0, 1550)
                Random_Size = randint(MinSize_Bonus, MaxSize_Bonus)
                Bonus_Circle = canvas.create_oval(x - Random_Size,
                                                  y - Random_Size,
                                                  x + Random_Size,
                                                  y + Random_Size,
                                                  outline="black",
                                                  fill=colours[randint(0, 4)])
                Bonus_List.append(Bonus_Circle)
                BonusSize_List.append(Random_Size)
                Bonus_Speed.append(randint(2, 5))

        def Cheat_Codes():
            firstname_info = firstname.get()
            if (firstname_info) == "score":
                global Score
                Score += 10
            elif (firstname_info) == "health":
                global Health
                Health += 25
            elif (firstname_info) == "fast":
                global speed
                speed += 5
            elif (firstname_info) == "bonus":
                global xas
                xas = 2
            elif (firstname_info) == "enemy":
                global maxi
                maxi += 50
# less likely for enemies to appear
            if firstname_entry != '':
                firstname_entry.delete(0, END)
        firstname_text1 = Label(text="Enter Code", bg="grey")
        firstname_text1.place(x=15, y=70)
        firstname = StringVar()
        firstname_entry1 = Entry(textvariable=firstname,
                                 width="30", bg="black", fg="white")
        firstname_entry1.place(x=15, y=100)
        register = Button(text="Confirm", width="30", height="2",
                          command=Cheat_Codes, bg="grey")
        register.place(x=15, y=150)

        def Enemy():

            pos = canvas.coords(SpaceCraft)
            x = randint(width_c, (width_c)+100)
            y = randint(pos[1]-100, pos[1]+100)
            Random_Size = randint(15, 20)
            Enemy_Circle = canvas.create_oval(x - Random_Size,
                                              y - Random_Size,
                                              x + Random_Size, y +
                                              Random_Size,
                                              outline="black", fill="red")
            Enemy_List.append(Enemy_Circle)
            EnemySize_List.append(Random_Size)
            Enemy_Speed.append(randint(4, 8))

        def move_mechanics():
            width1 = window.winfo_screenwidth()
            for i in range(len(Bonus_List)):
                canvas.move(Bonus_List[i], 0, Bonus_Speed[i])
            for i in range(len(Enemy_List)):
                canvas.move(Enemy_List[i], -Enemy_Speed[i], 0)

        def del_bonus(i):
            del BonusSize_List[i]
            del Bonus_Speed[i]
            canvas.delete(Bonus_List[i])
            del Bonus_List[i]

        def del_Enemy(i):
            del EnemySize_List[i]
            del Enemy_Speed[i]
            canvas.delete(Enemy_List[i])
            del Enemy_List[i]

        def distance(x, y):
            x1, y1 = get_coords(x)
            x2, y2 = get_coords(y)
            return sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

        def get_coords(value):
            pos = canvas.coords(value)
            x = (pos[0] + pos[2]) / 2
            y = (pos[1] + pos[3]) / 2
            width_c = str(window.winfo_screenwidth())
            height_c = str(window.winfo_screenheight())
            return x, y

        def EndGame():
            canvas.delete(ALL)
            global register, Score

            Score -= 10
            if Score < 0:
                Score = 0
            register.destroy()
            wait = canvas.create_text(350, 20, fill="white",
                                      font="Helvetica 20",
                                      text="       YOU GOT " + str(Score) +
                                      " PLEASE ENTER YOUR USERNAME BELOW!")
            canvas.delete("ALL")
            EndGame1()

        def EndGame1():
            global Save1, Score
            Save1.destroy()

            def save_info():
                firstname_info = firstname.get()
                text = canvas.create_text(325, 270, fill="white",
                                          font="Helvetica 20",
                                          text="Thank you, "+firstname_info +
                                          " would you like to play again?")
                file = open("user.txt", "a")
                file.write("\n"+firstname_info+"  Score:")
                global Score
                with open("user.txt", "a") as myfile:
                    myfile.write(str(Score))
                    file.close()
                    Score = 0
                firstname_entry.delete(0, END)
            global firstname_text, register1, firstname_entry
            firstname_text = Label(text="Username", bg="grey")
            firstname_text.place(x=15, y=70)
            firstname = StringVar()
            firstname_entry = Entry(textvariable=firstname,
                                    width="30", bg="black", fg="white")
            firstname_entry.place(x=15, y=100)
            register1 = Button(canvas, text="Save", width="30",
                               height="2", command=save_info, bg="grey")
            register1.place(x=15, y=150)

            def Quit(event):
                window.destroy()
                raise SystemExit
            global begin, buttonStart, Health, begin1
            global buttonStart1, speed

            def Main2(event):
                global Score
                Score = 0
                Main(event)

            begin = PhotoImage(file="loop.png")
            buttonStart = Label(window, image=begin)
            buttonStart.place(x=100, y=400)
            Health = 100
            speed = 10
            buttonStart.bind('<ButtonPress-1>', Main2)

            begin1 = PhotoImage(file="exit.png")
            buttonStart1 = Label(window, image=begin1)
            buttonStart1.place(x=300, y=400)

            buttonStart1.bind('<ButtonPress-1>', Quit)

            global leader
            global buttonLeader
            leader = PhotoImage(file="leader1.png")
            buttonLeader = Label(window, image=leader)
            buttonLeader.place(x=500, y=400)
            buttonLeader.bind('<ButtonPress-1>', High_Score)
            window.mainloop()

        def collision_enemy1():
            global Health
            Enemy_Score = 0
            for item in range(len(Enemy_List) - 1, - 1, - 1):
                if distance(Armour, Enemy_List[item]) < (15 +
                                                         EnemySize_List[item]):
                    del_Enemy(item)
                    Health -= 25
                    Enemy_Score -= 10
                    if Health == 0:
                        EndGame()
            return Enemy_Score

        def collision():
            User_Score = 0
            for item in range(len(
                                 Bonus_List) - 1, - 1, - 1):
                if distance(Armour, Bonus_List[item]) < (15 +
                                                         BonusSize_List[item]):
                    User_Score += (BonusSize_List[item] + Bonus_Speed[item])
                    del_bonus(item)
            return User_Score

        canvas.create_text(50, 30, text="SCORE", fill="white")
        st = canvas.create_text(50, 50, fill="white")
        st1 = canvas.create_text(100, 50, fill="white")
        canvas.create_text(100, 30, text="ARMOUR", fill="white")

        def Display_Score(Score):
            canvas.itemconfig(st, text=str(Score))

        def Display_Armour(Health):
            canvas.itemconfig(st1, text=str(Health))

        while True:
            global bonusChance
            if randint(1, bonusChance) == 1:
                Bonus()
            if randint(1, maxi) == 1:
                Enemy()

            collision()
            collision_enemy1()
            move_mechanics()
            Score += collision() + collision_enemy1()
            if Score < 0:
                Score = 0
            if Score >= 100:
                maxi = 60
                bonusChance += 50
            if Score >= 250:
                maxi = 50
                bonusChance += 50
            Display_Score(Score)
            Display_Armour(Health)
            window.update()
            sleep(0.01)

    def clicked(event):
        width_c = window.winfo_screenwidth()
        height_c = window.winfo_screenheight()
        window1 = Tk()
        canvas1 = Canvas(window, width=width_c, height=height_c, bg="black")
        canvas1.pack()
        window.mainloop()

    def Save2(event):
        f = open("save.txt", "w")
        f.write(str(Score) + " " + str(Health))
        f.close()
        wait = canvas.create_text(800, 150, fill="white",
                                  font="Helvetica 20",
                                  text="GAME SAVED")
        canvas.update()
        canvas.delete(wait)
        sleep(1)

    def Load2(event):

        try:
            with open("save.txt", "r") as myfile:
                data = myfile.readlines()
                input = str(data).replace("'", "")
                x = input.split()
                a = x[0]
                a1 = a.replace("[", "")
                b = x[1]
                b1 = b.replace("]", "")
                global Health, Score
                Score = int(a1)
                Health = int(b1)
                wait = canvas.create_text(800, 150, fill="white",
                                          font="Helvetica 20",
                                          text="GAME LOADED")

                if Health == 0:
                        EndGame()
                canvas.update()
                canvas.delete(wait)
                sleep(2)

        except:
                wait = canvas.create_text(800, 150, fill="white",
                                          font="Helvetica 20",
                                          text="NO SAVE GAME DATA")
                canvas.update()
                canvas.delete(wait)
                sleep(2)

    def Boss_Key(event):
        canvas.delete(ALL)
        Save2(event)
        work = PhotoImage(file="capture.png")
        Spreadsheet = Label(window, image=work).place(x=0, y=0)
        window.mainloop()

    def clicked1(event):
        global a, b, c, d, begin, buttonStart, buttonStart1
        if buttonStart1 != '':
            buttonStart1.destroy()
        a = "Down"
        b = "Up"
        c = "Left"
        d = "Right"
        canvas.delete(ALL)
        begin = PhotoImage(file="begin.png")
        buttonStart = Label(window, image=begin)
        buttonStart.place(x=100, y=400)
        buttonStart.bind('<ButtonPress-1>', Main)
        window.mainloop()

    def clicked2(event):
        global a, b, c, d, begin1, buttonStart1, buttonStart
        if buttonStart:
            buttonStart.destroy()
        a = "s"
        b = "w"
        c = "a"
        d = "d"
        canvas.delete(ALL)
        begin1 = PhotoImage(file="begin.png")
        buttonStart1 = Label(window, image=begin)
        buttonStart1.place(x=500, y=400)
        buttonStart1.bind('<ButtonPress-1>', Main)
        window.mainloop()

    def controls(a, b, c, d):
        if a == "":
            a = "Down"
            b = "Up"
            c = "Left"
            d = "Right"
        if event.keysym == a:
            canvas.move(SpaceCraft, 0, speed)
            canvas.move(Armour, 0, speed)

        elif event.keysym == b:
            canvas.move(SpaceCraft, 0, -speed)
            canvas.move(Armour, 0, -speed)
        elif event.keysym == c:
            canvas.move(SpaceCraft, -speed, 0)
            canvas.move(Armour, -speed, 0)
        elif event.keysym == d:
            canvas.move(SpaceCraft, speed, 0)
            canvas.move(Armour, speed, 0)

        elif event.keysym == "p":
            time.sleep(1)
        elif event.keysym == "space":
            pos = canvas.coords(SpaceCraft)
            canvas.move(Bullet, pos[0]+22, pos[1]+6)
            offset = Bullet, pos[0]+22, pos[1]+6
            window.after(100, shooting_after)

    def control_menu(event):
        global keys, keys2, wasd, updown
        global Logo1
        global buttonStart
        global buttonLeader
        global buttonControl
        Logo1.destroy()
        buttonStart.destroy()
        buttonLeader.destroy()
        buttonControl.destroy()
        canvas.delete(ALL)
        text = canvas.create_text(325, 70,
                                  fill="white", font="Times 30 bold",
                                  text="Please select your controls")
        keys = PhotoImage(file="keys.png")
        updown = Label(window, image=keys)
        updown.place(x=100, y=100)
        updown.bind('<ButtonPress-1>', clicked1)
        keys2 = PhotoImage(file="wasd.png")
        wasd = Label(window, image=keys2)
        wasd.place(x=500, y=100)
        wasd.bind('<ButtonPress-1>', clicked2)
        window.mainloop()

    def High_Score(event):
        global Logo1
        global buttonStart
        global buttonLeader
        global buttonControl, firstname_entry, register
        global firstname_text, firstname_text1
        global register1, firstname_entry1, begin1, buttonStart1
        if firstname_entry != '':
            firstname_entry.destroy()
        if firstname_entry1 != '':
            firstname_entry1.destroy()
        if register != '':
            register.destroy()
        if register1 != '':
            register1.destroy()
        if firstname_text != '':
            firstname_text.destroy()
        if firstname_text1 != '':
            firstname_text1.destroy()
        if buttonStart1 != '':
            buttonStart1.destroy()
        Logo1.destroy()
        buttonStart.destroy()
        buttonLeader.destroy()
        buttonControl.destroy()

        def Quit(event):
            window.destroy()
            raise SystemExit
        canvas.delete(ALL)
        begin = PhotoImage(file="play2.png")
        buttonStart = Label(window, image=begin)
        buttonStart.place(x=100, y=400)

        begin1 = PhotoImage(file="exit.png")
        buttonStart1 = Label(window, image=begin1)
        buttonStart1.place(x=300, y=400)
        buttonStart1.bind('<ButtonPress-1>', Quit)
        buttonStart.bind('<ButtonPress-1>', Main)
        file = open("user.txt", "a")
        file.write("\n"+"Default  Score:"+str(0))
        file.write("\n"+"Default  Score:"+str(0))
        file.write("\n"+"Default  Score:"+str(0))
        file.write("\n"+"Default  Score:"+str(0))
        file.write("\n"+"Default  Score:"+str(0))
        text = canvas.create_text(325, 70, fill="white",
                                  font="Times 50 bold", text="LEADERBAORD")
        with open("user.txt") as file:
            High_Score = file.readlines()
        High_Score = ([line.rstrip('\n') for line in open("user.txt")])
        list2 = list(filter(None, High_Score))

        All_Scores = []
        Alll_Scores = []

        for item in list2:
            Alll_Scores.append(item)
            xa = item[item.find(':'):]
            x = xa[1:]
            All_Scores.append(int(x))

        Alll_Scores.sort(key=lambda x: int(''.join(filter(str.isdigit, x))))
        top5 = len(Alll_Scores)
        Top5 = (Alll_Scores[top5-5:top5])
        text = canvas.create_text(225, 200, fill="white",
                                  font="Helvetica 20 ",
                                  text="1." + Top5[4] + "\n2." +
                                  Top5[3] + "\n3." + Top5[2] + "\n4." +
                                  Top5[1] + "\n5." + Top5[0])
        window.mainloop()
    canvas = Canvas(window, relief=FLAT, background="black", width=1000,
                    height=550)
    canvas.pack(fill="both", expand=True)
    keys = PhotoImage(file="financial.png")
    updown = Button(window, image=keys)
    updown.place(x=800, y=0)
    updown.bind('<ButtonPress-1>', Boss_Key)
    global Logo
    global Logo1
    Logo = PhotoImage(file="logo.png")
    Logo1 = Label(window, image=Logo)
    Logo1.place(x=275, y=50)
    global begin
    global buttonStart
    begin = PhotoImage(file="begin.png")
    buttonStart = Button(window, image=begin)
    buttonStart.place(x=450, y=225)
    buttonStart.bind('<ButtonPress-1>', Main)

    global leader
    global buttonLeader
    leader = PhotoImage(file="leader.png")
    buttonLeader = Button(window, image=leader)
    buttonLeader.place(x=450, y=300)
    buttonLeader.bind('<ButtonPress-1>', High_Score)

    global Control_Menu
    global buttonControl
    Control_Menu = PhotoImage(file="control.png")
    buttonControl = Button(window, image=Control_Menu)
    buttonControl.place(x=450, y=375)
    buttonControl.bind('<ButtonPress-1>', control_menu)

    global Load
    global Load1
    Load = PhotoImage(file="load.png")
    Load1 = Label(window, image=Load)
    Load1.place(x=900, y=0)
    Load1.bind('<ButtonPress-1>', Load2)
    window.mainloop()

main()
